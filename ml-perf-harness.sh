#!/bin/bash

# Read a config file with a set of tunables.  Apply those tunables to
# the running kernel, and then run the benchmark.  Benchmarks are
# expected to output a single, continuous value.

# General notes on bash/shell-isms that may not be familiar to the reader:
#
# The tunable variables (KYBER_TUNABLES, DEADLINE_TUNABLES, etc) are
# the uppercase of the tunable name/file.  For example, the
# configuration parameter READ_LAT_NSEC can be found in the file
# /sys/block/<dev>/queue/iosched/read_lat_nsec.  Thus, we can get the
# tunable name by lower-casing the variable name, which is what the
# ",," does in the ${!opt,,} expression.
#
# The script also makes use of the ${opt^^} syntax, which uppercases
# the variable.
#
# ${!<var>} is a way to indirectly reference a variable (IOW, the
# variable contains a variable name).  For example, KYBER_TUNABLES
# contains READ_LAT_NSEC.  In order to read the value of the parameter,
# we need $READ_LAT_NSEC.  That's what the ${!var} syntax allows--an
# indirect expansion.

# TODO:
# - save off existing parameters, and restore after benchmark run

# pull in integer limit definitions
source limits.sh

# before we source the config file, save off the list of shell
# variables.  We'll use this to output the list of variables declared in
# the configuration file.  See list_params().
SHELL_VARS="`set -o posix ; set`"

# read configuration
source ml-perf-harness.conf

# Get a list of currently set shell variables (set -o posix; set),
# and filter out the variables that existed before we sourced the
# config file (SHELL_VARS).
CONFIG_VARS="`grep -vFe "$SHELL_VARS" <<<"$(set -o posix ; set)" | \
		grep -vE \(^BASH_\|^FUNCNAME=\) | cut -d= -f1`"

# tunables specific to each io scheduler
KYBER_TUNABLES="READ_LAT_NSEC WRITE_LAT_NSEC"
BFQ_TUNABLES="BACK_SEEK_MAX BACK_SEEK_PENALTY FIFO_EXPIRE_SYNC FIFO_EXPIRE_ASYNC LOW_LATENCY MAX_BUDGET SLICE_IDLE SLICE_IDLE_US TIMEOUT_SYNC STRICT_GUARANTEES"
DEADLINE_TUNABLES="FIFO_BATCH READ_EXPIRE WRITE_EXPIRE WRITES_STARVED FRONT_MERGES READ_LAT_NSEC WRITE_LAT_NSEC"
NONE_TUNABLES=""

# map from tunable name to io scheduler
declare -A iosched_tunables
iosched_tunables["READ_LAT_NSEC"]="deadline kyber"
iosched_tunables["WRITE_LAT_NSEC"]="deadline kyber"
iosched_tunables["BACK_SEEK_MAX"]="bfq"
iosched_tunables["BACK_SEEK_PENALTY"]="bfq"
iosched_tunables["FIFO_EXPIRE_SYNC"]="bfq"
iosched_tunables["FIFO_EXPIRE_ASYNC"]="bfq"
iosched_tunables["LOW_LATENCY"]="bfq"
iosched_tunables["MAX_BUDGET"]="bfq"
iosched_tunables["SLICE_IDLE"]="bfq"
iosched_tunables["SLICE_IDLE_US"]="bfq"
iosched_tunables["TIMEOUT_SYNC"]="bfq"
iosched_tunables["STRICT_GUARANTEES"]="bfq"
iosched_tunables["FIFO_BATCH"]="deadline"
iosched_tunables["READ_EXPIRE"]="deadline"
iosched_tunables["WRITE_EXPIRE"]="deadline"
iosched_tunables["WRITES_STARVED"]="deadline"
iosched_tunables["FRONT_MERGES"]="deadline"

# request queue sysfs tuning knobs
QUEUE_TUNABLES_BIO="MAX_SECTORS_KB READ_AHEAD_KB"
QUEUE_TUNABLES_REQUEST="$QUEUE_TUNABLES_BIO NR_REQUESTS WBT_LAT_USEC"

# sysctl tunables
VM_SYSCTL="DIRTY_RATIO DIRTY_BACKGROUND_RATIO SWAPPINESS DIRTY_EXPIRE_CENTISECS DIRTY_WRITEBACK_CENTISECS DIRTYTIME_EXPIRE_SECONDS OVERCOMMIT_RATIO"
KERNEL_SYSCTL="SCHED_MIN_GRANULARITY_NS SCHED_WAKEUP_GRANULARITY_NS SCHED_MIGRATION_COST_NS"

# If device-mapper is present, TOP_LEVEL_DEV is the dm device (dm-0),
# and TEST_DEVS will contain the list of component devices.  If the
# test directory is not backed by a device-mapper target, then
# TOP_LEVEL_DEV will be the sd device, and TEST_DEVS will also be that
# same device.  In all cases, the device names are the final path
# component (i.e. sda, not /dev/sda), and is the whole device (not a
# partition).
TOP_LEVEL_DEV=
TEST_DEVS=

SAVED_TUNED_PROFILE=

function cleanup()
{
	[ -n "$SAVED_TUNED_PROFILE" ] &&
	    tuned-adm profile $SAVED_TUNED_PROFILE >&/dev/null
}

# Checker functions are used to ensure there isn't an invalid combination
# of ioscheduler and parameters (for example, setting bfq parameters for
# the kyber io scheduler).
function check_iosched_kyber()
{
	local tunable

	for tunable in $BFQ_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
	for tunable in $DEADLINE_TUNABLES; do
		# there are some tunables that are valid for both kyber
		# and deadline
		if [[ ${iosched_tunables[$tunable]} =~ "kyber" ]]; then
		    continue;
		fi
		[ -n "${!tunable}" ] && exit 1
	done
}

function check_iosched_bfq()
{
	local tunable

	for tunable in $DEADLINE_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
	for tunable in $KYBER_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
}

function check_iosched_deadline()
{
	local tunable

	for tunable in $BFQ_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
	for tunable in $KYBER_TUNABLES; do
		# there are some tunables that are valid for both kyber
		# and deadline
		if [[ ${iosched_tunables[$tunable]} =~ "deadline" ]]; then
			continue;
		fi
		[ -n "${!tunable}" ] && exit 1
	done
}

function check_iosched_none()
{
	for tunable in $BFQ_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
	for tunable in $DEADLINE_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
	for tunable in $KYBER_TUNABLES; do
		[ -n "${!tunable}" ] && exit 1
	done
}

# Given a directory, figure out the backing device.  In the case of
# device-mapper devices, that is usually a symbolic link.  Dereference
# the link, and return the actual device name (dm-*).
# Returns a full path to the device.
function device_for()
{
	local directory=$1
	local devnode

	devnode=$(df $directory | tail -1 | cut -d' ' -f1)
	if [ -h $devnode ]; then
		devnode=$(readlink -f $devnode)
	fi
	# strip off partition numbers before returning
	local whole=$(lsblk -o PKNAME $devnode | tail -1)
	[ -n "$whole" ] && devnode=$whole
	echo $devnode
}

# returns a list of device names (without full paths)
function get_test_devs()
{
	local parent_dev=$1
	local dev_list dev blkdevs

	# most stacked devices (device mapper) are bio-based, which means
	# they do not have an io scheduler.  In those cases, we need to
	# change the iosched of the underlying devices.  Detect that case
	# here, and loop over the constituent devices.
	#
	# the dmsetup output prints each device in parenthesis:
	# $dmsetup deps -o blkdevname /dev/dm-0
	# 2 dependencies	: (nvme0n1) (pmem0)
	# pick out just the device names.
	blkdevs=$(dmsetup deps -o blkdevname $parent_dev 2>/dev/null | cut -d: -f 2 | tr -d '()')
	if [ -n "$blkdevs" ]; then
		# blkdev names include the partition (e.g. sda3).  We need the
		# name of the whole block device (sda).
		for dev in $blkdevs; do
			local whole=$(lsblk -o PKNAME /dev/$dev | tail -1)
			[ -n "$whole" ] && dev=$whole
			dev_list="$dev_list $dev"
		done
	else
		dev_list=$(basename $parent_dev)
	fi
	echo $dev_list
}

function get_set_iosched_tunables()
{
	local iosched=$1
	local iosched_dir=$2
	local set=$3
	local tunables_var=${iosched^^}_TUNABLES
	local opt

	for opt in ${!tunables_var}; do
		if [ $set -eq 1 ] ; then
			[ -n "${!opt}" ] && echo ${!opt} > ${iosched_dir}/${opt,,}
			if [ $? -ne 0 ]; then
				echo "Failed to set ${iosched_dir}/${opt,,} to ${!opt}"
				exit 1
			fi
		else
			cat ${iosched_dir}/${opt,,}
		fi
	done
}

# Set the configured I/O scheduler, and apply any scheduler-specific
# parameters.
# TODO: for a request-based top-level device (such as dm-multipath),
# we want to set the scheduler for the dm device, and set the scheduler
# to none for the component devices.
function setup_iosched()
{
	local dev

	for dev in $TEST_DEVS; do
		echo $IOSCHED > /sys/block/$dev/queue/scheduler
		get_set_iosched_tunables $IOSCHED "/sys/block/$dev/queue/iosched" 1
	done
}

function list_first()
{
	echo $1
}

function print_iosched()
{
	local dev=$(list_first $TEST_DEVS)

	cat /sys/block/$dev/queue/scheduler
	get_set_${IOSCHED}_params /sys/block/$dev/queue/iosched 0
}

function get_device_type()
{
	local dev=$1

	# For now, just check to see if the device supports an I/O
	# scheduler.  If it does, it is request-based.  There may be a
	# better way to do this.
	nr_scheds=$(cat /sys/block/$dev/queue/scheduler | wc -w)
	if [ $nr_scheds -gt 1 ]; then
		echo request
	else
		echo bio
	fi
}

# Some request queue parameters are only valid for request-based
# devices, while others can be applied to both request-based and
# bio-based devices.  Determine the type of device for both the
# top-level (TOP_LEVEL_DEV) and the component devices (TEST_DEVS), and
# apply tunings accordingly.
function setup_request_queue()
{
	for dev in $TOP_LEVEL_DEV $TEST_DEVS; do
		local tunables
		case $(get_device_type $dev) in
		request)
			tunables=QUEUE_TUNABLES_REQUEST
			;;
		bio)
			tunables=QUEUE_TUNABLES_BIO
			;;
		esac

		for opt in ${!tunables}; do
			local queue_dir=/sys/block/$dev/queue
			[ -n "${!opt}" ] && echo ${!opt} > ${queue_dir}/${opt,,}
			if [ $? -ne 0 ]; then
				echo "Failed to set ${queue_dir}/${opt,,} to ${!opt}"
			    exit 1
			fi

		done
	done
}

function setup_tuned_profile()
{
	SAVED_TUNED_PROFILE=$(tuned-adm active | cut -d: -f2 | tr -d ' ')
	mkdir -p /etc/tuned/benchmark
	cat<<EOF>/etc/tuned/benchmark/tuned.conf
[cpu]
governor=performance
energy_perf_bias=performance
min_perf_pct=100
EOF

	tuned-adm profile benchmark
}

# apply the sysctl tunings
function setup_sysctl()
{
	for opt in $VM_SYSCTL; do
		[ -n "${!opt}" ] && echo ${!opt} > /proc/sys/vm/${opt,,}
		if [ $? -ne 0 ]; then
			echo "Failed to set /proc/sys/vm/${opt,,} to ${!opt}"
			exit 1
		fi
	done
	for opt in $KERNEL_SYSCTL; do
		[ -n "${!opt}" ] && echo ${!opt} > /proc/sys/kernel/${opt,,}
		if [ $? -ne 0 ]; then
			echo "Failed to set /proc/sys/kernel/${opt,,} to ${!opt}"
			exit 1
		fi
	done
}

function apply_tunings()
{
	setup_iosched
	setup_request_queue
	setup_sysctl
	setup_tuned_profile
}

# Must be called after applying a configuration.  This will print out the
# settings as they exist on the machine.  It is useful for verifying the
# configuration was properly applied (Debugging), and also for reproducing
# a test run.  For example, you could provide an incomplete configuration,
# and this function will then print out a full configuration representing
# the test run.
function print_config()
{
	print_iosched
	print_request_queue
	print_sysctl
}

function list_params()
{
	for v in $CONFIG_VARS; do
		if [[ "$v" =~ "__MIN" ]] || [[ "$v" =~ "__MAX" ]] || \
		   [[ "$v" =~ "__TYPE" ]]; then
			echo $v=${!v}
		else
			echo $v
		fi
	done
}

function check_int()
{
	local opt=$1
	local min=$opt"__MIN"
	local max=$opt"__MAX"
	if [ ${!opt} -lt ${!min} ] || \
	       [ ${!opt} -gt ${!max} ]; then
	    exit 1
	fi
}

# Each list type config option is represented by 3 script variables.
# For example:
#   IOSCHED=
#   IOSCHED__TYPE=list
#   IOSCHED__LIST="bfq deadline kyber none"
# This function makes sure that the option is set to a valid value.  For
# the above example, it ensures that $IOSCHED contains one of bfq, deadline,
# kyber or none.
function check_list()
{
	local opt=$1
	local valid_values=$opt"__LIST"

	if [[ ! ${!valid_values} =~ ${!opt} ]]; then
		exit 1
	fi
}

function check_bool()
{
	local opt=$1

	[[ "${!opt}" =~ ^[01]?$ ]] || exit 1
}

function check_ranges()
{
	local opt

	for opt in $CONFIG_VARS; do
		: echo $opt
		# skip over our type information
		[[ $opt =~ "__MIN" ]] && continue
		[[ $opt =~ "__MAX" ]] && continue
		[[ $opt =~ "__TYPE" ]] && continue
		[[ $opt =~ "__LIST" ]] && continue

		# skip over unset options
		[ -z ${!opt} ] && continue

		local typename=$opt"__TYPE"
		case ${!typename} in
		int)
			check_int $opt
			;;
		list)
			check_list $opt
			;;
		bool)
			check_bool $opt
			;;
		esac
	done
}

function check_iosched()
{
	check_iosched_${IOSCHED}
}

# ensure the configuration does not contain invalid options, or invalid
# combinations of options.  If an error condition is encountered, the
# script terminates with exit 1.
function check_config()
{
	check_ranges
	check_iosched
	echo "Configuration is valid"
}

function drop_caches()
{
	echo 3 > /proc/sys/vm/drop_caches
}

# XXX do we want to setup a directory for the test?  what options
# does the test script require to run?
function run_test()
{
	local script=$1
	local count=$2

	for i in $(seq 1 $count); do
		drop_caches
		bash $script
	done
}

function usage()
{
	prog=$1

	echo -e "Usage: $prog [OPTION]..."
	echo -e "-c          \tcheck config file for invalid option combinations."
	echo -e "            \tReturns 0 for a valid config, non-zero otherwise."
	echo -e "-n #        \tRun the benchmark # times.  The script will output"
	echo -e "            \tone line per run."
	echo -e "-l          \tPrint out all configuration parameters, their types,"
	echo -e "            \tand valid values."
	echo -e "-t <script> \tRun test script."
}

trap cleanup EXIT

# setup the global device names
TOP_LEVEL_DEV=$(basename $(device_for $PWD))
TEST_DEVS=$(get_test_devs /dev/$TOP_LEVEL_DEV)

# typical invocation: $prog -t <test script>
# will cause this script to apply a particular tuning config ($prog.conf),
# and then run the test (up to n times), printing the test output for each
# run to stdout.
num_runs=1
while getopts "cn:lt:" arg; do
	case $arg in
	c)
		apply_tunings
		check_config
		exit 0
		;;
	n)
		num_runs=$OPTARG
		;;
	l)
		list_params
		exit 0
		;;
	t)
		test_script=$OPTARG
		;;
	\?)
		usage $(basename $0)
		exit 1
		;;
	esac
done

if [ -z "$test_script" ]; then
	usage
	exit 1
fi

apply_tunings
run_test $test_script $num_runs
